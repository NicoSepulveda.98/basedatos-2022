#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include "hebra.h"

using namespace std;

Hebra::Hebra() {

}

float Hebra::calc_time(float time_i, float time_f) {
    
    float execution_time = (time_f - time_i) / CLOCKS_PER_SEC;

    return execution_time;
}

/* Calcular lineas de un archivo */
void *Hebra::Row_manipulation(void *param) {
    
    file_arch *manage_data;
    manage_data = (file_arch *)param;
    char *str;
    int count_line = 0; /* Contador línea */
    /* Recorre el archivo */
    while (!manage_data->arch.eof()) {
        manage_data->arch.getline(str, 100000);
        /* Suma una linea */
        count_line ++;
    }
    manage_data->sum_row = count_line;
    cout << endl;
    cout << count_line << " Lines" << endl;

    pthread_exit(0);
}

/* Manipulación de las palabras */
void *Hebra::Word_manipulation(void *param) {

    file_arch *manage_data;
    manage_data = (file_arch *)param;
    char str[100000];
    int count_word = 0; /* Contador de palabras */
    
    while (!manage_data->arch.eof()) {
        /* Reconocimiento de una palabra */
        manage_data->arch >> str;
        /* Se suma una palabra */
        count_word ++;
    }
    manage_data->sum_word = count_word;
    cout << count_word << " Words" << endl;
    
    pthread_exit(0);
}

/*  Manipulación de los caracteres */
void *Hebra::Characters_manipulation(void *param) {

    file_arch *manage_data;
    manage_data = (file_arch *)param;
    int count_characters = 0; /* Contador de caracteres */
    string str;
    
    while (getline(manage_data->arch, str)) {
        /*  Se igual una variable al largo del string */
        int len = str.length();
        /* Para luego crear un ciclo de esa para ir */
        for (int i = 0; i < str.length(); i++) {
            if (str.at(i) == ' ') {
                /* Sumando en el marcador de caracteres */
                len --;
            }
        }
        count_characters = count_characters + len;
    }
    /* Se iguala los caracteres para almacenarlo y utilizarlo en el main */
    manage_data->sum_characters = count_characters;
    cout << count_characters << " Characters" << endl;

    pthread_exit(0);
}
