#ifndef HEBRA_H
#define HEBRA_H
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include "hebra.h"
#include <fstream>

using namespace std;

struct file_arch {
    
    ifstream arch;

    int sum_row = 0;
    int sum_word = 0;
    int sum_characters = 0;

};

class Hebra {
    private:

    public:
        /* Construtor */
        Hebra();
        /* Metodos */
        void *Row_manipulation(void *param);
        void *Word_manipulation(void *param);
        void *Characters_manipulation(void *param);
        float calc_time(float time_i, float time_f);
};

#endif
