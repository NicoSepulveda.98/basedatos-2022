#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include "hebra.h"

using namespace std;

void *analyze_lines (void *param) {
    
    file_arch *manage_data;
    manage_data = (file_arch *)param;
    Hebra hebra;
    hebra.Row_manipulation(manage_data);
    return NULL;
}

void *analyze_words (void *param) {

    file_arch *manage_data;
    manage_data = (file_arch *)param;
    Hebra hebra;
    hebra.Word_manipulation(manage_data);
    return NULL;
}

void *analyze_characters (void *param) {

    file_arch *manage_data;
    manage_data = (file_arch *)param;
    Hebra hebra;
    hebra.Characters_manipulation(manage_data);
    return NULL;
}


int main (int argc, char *argv[]) {
    
    if (argc < 2) {
        cout << "Please specify the text file." << endl;
    
        return -1;
    }
    
    /* Llamar al constructor */
    Hebra hebra;

    pthread_t threads[argc*3-1];
    //pthread_attr_t attr;   
    file_arch manage_data;
    /* Inicio del tiempo para demostrar la tardanza de ejecución */
    clock_t time_i = clock();
    
    /* Variables para la suma de lineas, palabras y caracteres totales */
    int sum_row = 0;
    int sum_word = 0;
    int sum_characters = 0;
    cout << "\n";
    cout << "|---------------[*]---------------|" << endl;
    cout << "\n| Welcome to Nicolas file reader! |" << endl;
    cout << "\n";
    cout << "|---------------[*]---------------|" << endl;
    cout << endl << endl;
    
    for (int i = 1; i < argc; i++) {
        cout << "\nFile name: " << argv[i] << endl;
        /* LLamar a la estructurar para abrir el archivo */
        manage_data.arch.open(argv[i]);
        /* Crear hebra para contar lineas de archivos */
        pthread_create(&threads[i-1], NULL, analyze_lines, (void *)&manage_data);
        pthread_join(threads[i-1], NULL);
        manage_data.arch.close();
        sum_row = sum_row + manage_data.sum_row;
        
        manage_data.arch.open(argv[i]);
        /* Crear hebra para contar palabras de archivos */
        pthread_create(&threads[i-1],  NULL, analyze_words, (void *)&manage_data);
        pthread_join(threads[i-1], NULL);
        manage_data.arch.close();
        sum_word = sum_word + manage_data.sum_word;

        manage_data.arch.open(argv[i]);
        /* Crear hebra para contar caracteres de archivos */
        pthread_create(&threads[i-1],  NULL, analyze_characters, (void *)&manage_data);
        pthread_join(threads[i-1], NULL);
        manage_data.arch.close();
        sum_characters = sum_characters + manage_data.sum_characters;
    }
    cout << endl << endl;
    cout << " Total sum of lines: " << sum_row << endl;
    cout << " Total sum of words: " << sum_word << endl;
    cout << " Total sum of characters: " << sum_characters << endl << endl; 

    /* Fin del tiempo */
    clock_t time_f = clock();
    /* Se demuestra lo que tardó el programa */
    cout << "The execution time took is " << hebra.calc_time(time_i, time_f) << " s" << endl << endl;

    return 0;
}
