#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include "hebra.h"
#include <fstream>
#include <ctime>

using namespace std;

/* Constructor */
Hebra::Hebra() {
}

/* Calcular el tiempo de ejecución */
float Hebra::calc_time(float time_i, float time_f) {
    
    float execution_time = (time_f - time_i) / CLOCKS_PER_SEC;

    /* Retornar al tiempo de ejecución */
    return execution_time;
}

/*  Función int para contar las lineas y retornar a ese valor */
int Hebra::Row_manipulation(string arch) {
    
    ifstream file(arch);
    string str;
    int count_line = 0; /* Contador línea */
    
    /* Recorrido del archivo */
    while (getline(file, str)) {
        /* Se agrega una línea */
        count_line ++;
        for (int i = 0; i < str.length(); i++) {
            cout << str[i];
        }
        cout << endl;
    }
    cout << endl;
    cout << count_line << " Lines" << endl;

    /* Se retorna al valor del total de las lineas del archivo */
    return count_line;
}

/* Manipulación de las palabras */
int Hebra::Word_manipulation(string arch) {

    ifstream file(arch);
    char str[100000];
    int count_word = 0; /* Contador de palabras */
    
    while (!file.eof()) {
        /* Reconocimiento de una palabra */
        file >> str;
        /* Se suma una palabra */
        count_word ++;
    }
    cout << count_word << " Words" << endl;
    
    /* Se retorna al valor total de palabras de este archivo */
    return count_word;
}
/* Manipulación de los caracteres del archivo */
int Hebra::Characters_manipulation(string arch) {

    ifstream file(arch);
    int count_characters = 0; /* Contador de caracteres */
    string str;
    
    while (getline(file, str)) {
        int aux_num = str.length();
        for (int i = 0; i < str.length(); i++) {
            /* Se verifica si existe un espacio vacío en una posición con .at(i)  */
            if (str.at(i) == ' ') {
                /*  Para luego restar uno */
                aux_num -- ;
            }
        }
        count_characters = count_characters + aux_num;
    }
    cout << count_characters << " Characters" << endl;

    /* Se retorna al numero total de caracteres del archivo */
    return count_characters;
}



