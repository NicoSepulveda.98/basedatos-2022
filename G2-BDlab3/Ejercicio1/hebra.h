#ifndef HEBRA_H
#define HEBRA_H
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

using namespace std;

class Hebra {
    private:

    public:
        /* Constructor */
        Hebra();
        int Row_manipulation(string arch);
        int Word_manipulation(string arch);
        int Characters_manipulation(string arch);
        float calc_time(float time_i, float time_f);
};

#endif