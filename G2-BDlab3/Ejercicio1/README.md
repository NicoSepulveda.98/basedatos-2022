**Ejercicio Guia2-Lab3**

El siguiente programa consiste en utilizar la terminal para habilitar la información de un archivo de texto, permitiendo calcular el total de lineas, palabras y caracteres. La ventaja del programa es que permite la utilización de mas de un archivo de texto. Además entrega el tiempo que tardo en ejecutarse.

**Especificaciones técnicas**

Es necesario 'adjuntar' el archivo de texto a utilizar, en este caso esta compilado por Makefile, por ende un ejemplo de ejecución (existen 2 archivos de texto dentro la carpeta que puede utilizarse como referencia) es ./main palabras.txt . En caso de no entregar los parametros adecuados, el programa no se podrá ejecutar.

**Historia:**

Fue escrito y desarrollado por Nicolás Sepúlveda Falcón, utilizando para ello el lenguaje de programación C++. Se trata de un lenguaje robusto fuertemente equipado y que sigue un paradigma de programación orientada a objetos.

**Para empezar:**

Es requisito tener instalado **C++**, de preferencia compilador **g++ 9.3.0**, previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)

**Instalación:**

Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "basedatos-2022" en el directorio de su preferencia, el enlace HTTPS para clonar el programa ingresando a la terminal es el siguiente:
```
git clone https://gitlab.com/NicoSepulveda.98/basedatos-2022.git
```
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "basedatos-2022" especificamente en "G2-BDlab3/Ejercicio1" que contiene los ejercicios con sus respectivas clases en formato ".cpp" y ".h" y un Makefile.
3- Posteriormente, ya está usted preparado para ejecutar el programa. Puede ser lanzado desde una consola de su IDE de preferencia o desde una terminal en Linux

**Codificación:**

El programa soporta la codificación estándar UTF-8

**Construido con:**

Visual Studio Code, IDE utilizado por defecto para el desarrollo del proyecto.
Sitio web para descargar para Linux, Windows y Mac en https://code.visualstudio.com/download"

**Licencia:**

Este proyecto está sujeto bajo la Licencia GNU GPL v3.

**Autores y creadores del proyecto:**

> Nicolás Sepúlveda Falcón.
