#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include "hebra.h"


using namespace std;

int main (int argc, char *argv[]) {
    
    if (argc < 2) {
        cout << "Please specify the text file." << endl;
    
        return -1;
    }
    
    /* Llamar al constructor */
    Hebra hebra;
    ifstream arch;
    /* Inicio del tiempo para demostrar la tardanza de ejecución */
    clock_t time_i = clock();

    /* Variables para la suma de lineas, palabras y caracteres totales */
    int sum_row = 0;
    int sum_word = 0;
    int sum_characters = 0;
    cout << "\n";
    cout << "|---------------[*]---------------|" << endl;
    cout << "\n| Welcome to Nicolas file reader! |" << endl;
    cout << "\n";
    cout << "|---------------[*]---------------|" << endl;
    cout << endl << endl;
    
    for (int i = 1; i < argc; i++) {
        cout << "File name: " << argv[i] << endl;
        arch.open(argv[i]);
        /* Se llama a las funciones y el proceso de cada una de ellas
        con respecto al archivo y al retornar al valor se guarda en sus
        respectivas variables para sumarlas todas las lineas, palabras y caracteres */
        sum_row = sum_row + hebra.Row_manipulation(argv[i]);
        sum_word = sum_word + hebra.Word_manipulation(argv[i]);
        sum_characters = sum_characters + hebra.Characters_manipulation(argv[i]);
        arch.close();
        cout << endl << endl;
    }
    cout << " Total sum of lines: " << sum_row << endl;
    cout << " Total sum of words: " << sum_word << endl;
    cout << " Total sum of characters: " << sum_characters << endl << endl;

    /* Fin del tiempo */
    clock_t time_f = clock();
    /* Se demuestra lo que tardó el programa */
    cout << "The execution time took is " << hebra.calc_time(time_i, time_f) << " s" << endl << endl;
    
    return 0;
}
