#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include "Fork.h"
using namespace std;

/* Constructor */
Fork::Fork() {
}
    
/* Metodos */
void Fork::Create() {
  /* Crea el proceso hijo */
  pid = fork();
}
    
void Fork::run_Code(char **url) {
  /* Valida la creación del proceso */
  if (pid < 0) {
    cout << "\nParent: error\n";
    cout << endl;
  } 
  else if (pid == 0) {
    cout << "\nChild: Hello, World!" << endl;
    cout << "Child: Process ID: " << getpid() << endl;
    cout << "Downloading video and extracting audio...\n";
    execlp ("youtube-dl", "youtube-dl", "-x", "--audio-format", "mp3", url[1], "-o", "music.mp4", NULL);
  } 
  else {        
    wait(NULL);
    cout << "\nWork completed!\n";
    cout << "\nParent: Continue with process..." << endl;
    execlp ("cvlc", "cvlc", "music.mp3", NULL);
    cout << "\nParent: Child process waited for.\n";
  }
};