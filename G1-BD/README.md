**Ejercicio Guia1-Base de Datos**

El siguiente programa consiste en la utilización de dos procesos: Fork() y  execlp(). Esto permitirá la descarga de videos en youtube mediante _youtube-dl_ y el URL del video escogido, transformando el video en formato mp4 a mp3. Ademas de la participación del reproductor _VLC_, permitiendo la reproducción del video en formato mp3 a través de la terminal. 

**Especificaciones técnicas**

El método de resolución es mediante la terminal, es decir, al ejecutar el programa con ./main URL del video a tratar. Si no entrega ambos parametros, el programa automaticamente imprimirá un error, inhabilitando la utilización de los métodos.

**Historia:**

Fue escrito y desarrollado por Nicolás Sepúlveda Falcón, utilizando para ello el lenguaje de programación C++. Se trata de un lenguaje robusto fuertemente equipado y que sigue un paradigma de programación orientada a objetos.

**Para empezar:**

Es requisito tener instalado **C++**, de preferencia compilador **g++ 9.3.0**, previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)

**Instalación:**

Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "proyecto2021-2semestre" en el directorio de su preferencia, el enlace HTTPS para clonar el programa ingresando a la terminal es el siguiente:
```
https://gitlab.com/NicoSepulveda.98/basedatos-2022.git

```
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "basedatos-2022" especificamente en "G1-BD" que contiene los ejercicios con sus respectivas clases en formato ".cpp" y ".h" y un Makefile.
3- Posteriormente, ya está usted preparado para ejecutar el programa. Puede ser lanzado desde una consola de su IDE de preferencia o desde una terminal en Linux

**Codificación:**

El programa soporta la codificación estándar UTF-8

**Construido con:**

Visual Studio Code, IDE utilizado por defecto para el desarrollo del proyecto.
Sitio web para descargar para Linux, Windows y Mac en https://code.visualstudio.com/download"

**Licencia:**

Este proyecto está sujeto bajo la Licencia GNU GPL v3.

**Autores y creadores del proyecto:**

> Nicolás Sepúlveda Falcón.
