#ifndef FORK_H
#define FORK_H
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

using namespace std;
/* Creación de la clase */
class Fork {
  private:  
    pid_t pid;
    char **url;
  
  public:
    /* Constructor */
    Fork();
    void Create();
    void run_Code(char **url);
};

#endif