#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include "Fork.h"
using namespace std;

int main (int argc, char **argv) {
    
    cout << ("Parent: Hello, World!\n");
    if (argc < 2) {
    cout << "Please specify the address of the Video." << endl;
    cout << "Use: ./main (URL)" << endl;
    
    return -1;
    }
    /* Llamar al constructor */
    Fork fork;
    fork.Create();
    fork.run_Code(argv);
    
}